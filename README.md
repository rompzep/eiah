# Installation

Testé sous Debian et Ubuntu.

* Dépendances système  : `sudo apt-get install python3-venv python3-dev default-libmysqlclient-dev build-essential`
* Récupération du projet  : `git clone https://gitlab.com/Romreventon/eiah.git`
* Se déplacer dans le répertoire du projet : `cd eiah/eiah`
* Création de l'environnement python : `python3 -m venv .`
* Activation de l'environnement virtuel : `source bin/activate`
* Installation des dépendances de l'application web : `pip install -r requirements.txt`

# Lancement de l'application web

* Activer l'environnement virtuel si pas déjà fait.
* Se placer dans le répertoire du fichier `manage.py` et lancer le serveur : `./manage.py runserver`
* Ouvrir http://127.0.0.1:8000/indicateurs dans un navigateur web.

# Page d'accueil

![alt text](screenshot_1.png "Title Text")

## Clic sur l'irrégularité d'un utilisateur
Affiche les scores journaliers de l'activité d'un utilisateur 

![alt text](screenshot_2.png "Title Text")

