from django.shortcuts import render
from traceforum import traceforum_to_json, load_json
import numpy as np
from datetime import datetime

def index(request):
    """
    Pour extraire les données de traceforum dans le modèle générique (JSON) :

    - Il faut disposer d'une bdd MySQL de tracefoum configurée comme suit :
    host: localhost
    db name: traceforum
    user: root
    password: ~Root123

    - Puis éxécuter traceforum_to_json()

    NOTE: Voir le fichier traceforum.py pour le détail
    """

    # Charge les données du modèle générique dans un dictionnaire
    traces = load_json()

    l_user = {}
    l_act = {
        'Connexion':1,
        'Afficher une structure (cours/forum)':2,
        'Répondre à un message':30,
        'Afficher le fil de discussion':3,
        'Poster un nouveau message':50,
        "Afficher le contenu d'un message":5,
        'Bouger la scrollbar en bas - afficher la fin du message':3,
        'Bouger la scrollbar en bas':1,
        'Citer un message':15,
        'Upload un ficher avec le message':15,
        'Download un fichier dans le message':15,
        'Afficher une structure du cours':2}

    for t in traces:
        if t['Action']['Type']=='Connexion':
            if t['User'] not in l_user:
                l_user[t['User']]={"nb_con":1,"l_con":{}}

            else:
                l_user[t['User']]["nb_con"] +=1

    for t in traces:
        if t['User'] in l_user:

            if t['Action']['Date'] in l_user[t['User']]["l_con"]:
                l_user[t['User']]["l_con"][t['Action']['Date']] += l_act[t['Action']['Type']]
            else:
                l_user[t['User']]["l_con"][t['Action']['Date']] = l_act[t['Action']['Type']]

    names = [user for user in l_user]
    scores = [list(l_user[user]['l_con'].values()) for user in l_user]
    irregularite_scores = [int(np.std(score)) for score in scores]

    data_irregularite_scores = [{'name': user, 'y': irregularite, 'drilldown': user} for user,irregularite in zip(names,irregularite_scores)]
    data_scores = [{
        'name': user,
        'id': user,
        'data': [[d,s] for d,s in zip(list(l_user[user]['l_con'].keys()), list(l_user[user]['l_con'].values()))]
    } for user in names]

    dates = [list(l_user[user]['l_con'].keys()) for user in l_user]
    irregularite_dates = []
    for dates_user in dates:
        irregularite_dates_user = []
        for i in range(0,len(dates_user)-1):
            irregularite_dates_user.append((datetime.strptime(dates_user[i+1], "%Y-%m-%d") - datetime.strptime(dates_user[i], "%Y-%m-%d")).days)
        if irregularite_dates_user != []:
            irregularite_dates.append(int(np.std(irregularite_dates_user)))
        else:
            irregularite_dates.append(0)
    for user,ecart_type in zip(names,irregularite_dates):
        print('%s: %s' % (user, ecart_type))
    data_irregularite_dates = [{'name': user, 'y': irregularite, 'drilldown': user} for user,irregularite in zip(names,irregularite_dates)]

    context = {'names': names, 'data_scores': data_scores, 'data_irregularite_scores': data_irregularite_scores, 'data_irregularite_dates': data_irregularite_dates}
    return render(request, 'indicateurs/index.html', context=context)
