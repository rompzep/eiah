from django.apps import AppConfig


class IndicateursConfig(AppConfig):
    name = 'indicateurs'
