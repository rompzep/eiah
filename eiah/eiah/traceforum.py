import MySQLdb
import json

def trace_template():
    return {
        'CMCTool': {
            'Type': 'forum',
            'Name': 'traceforum'
        },
        'Message': {
            'Content': {
                'Text': None,
                'ContentID': None
            },
            'Date': None,
            'Sender': None,
            'Receiver': None,
            'Attributes': {
                'Title': None,
                'AttachedFile': None
            }
        },
        'User': None,
        'Action': {
            'Type': None,
            'Date': None
        }
    }

def traceforum_to_dict():
    user = "root"
    passwd = "~Root123"
    db = "traceforum"
    host = "localhost"

    connection = MySQLdb.connect(host=host, user=user, passwd=passwd, db=db)
    cursor = connection.cursor()
    cursor.execute("SELECT Utilisateur, Titre, Attribut, Date, Heure, Delai, Commentaire FROM transition")
    transitions = [transition for transition in cursor.fetchall()]
    cursor.close()
    connection.close()

    traces = []
    for transition in transitions:
        trace = trace_template()
        trace['User'] = transition[0]
        trace['Action']['Type'] = transition[1]
        trace['Action']['Date'] = transition[3]
        traces.append(trace)
    return traces

def traceforum_to_json(file='traceforum.json'):
    traces = traceforum_to_dict()
    with open(file, 'w') as f:
        json.dump(traces, f, indent=4, default=str)

def load_json(file='traceforum.json'):
    with open(file) as f:
        return json.load(f)
